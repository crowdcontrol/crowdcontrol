from flask import Blueprint, render_template, abort
from app.models.Page import Page
from app.models.Menu import Menu
from app.models.SiteSetting import SiteSetting
import commonmark

pages_blueprint = Blueprint('pages', __name__, template_folder='templates')


@pages_blueprint.route('/', defaults={'slug': ''})
@pages_blueprint.route('/<path:slug>')
def page(slug):
    """ This is the main route of the site. It handles the index and all other 'static' pages. """

    # Some sensible defaults
    the_page = {'Title': "", 'Content': ""}
    template_path = "site/index.html"

    # Init menu to a blank one
    menu = Menu

    # Get all the site settings
    site_settings = SiteSetting.query.all()
    settings = {}
    for setting in site_settings:
        settings[setting.name] = setting.value

    # Markdown Parser and Renderer
    parser = commonmark.Parser()
    renderer = commonmark.HtmlRenderer()

    if slug == "":
        home_page = Page.query.filter_by(is_homepage=True).first()
        if home_page is not None:
            parsed = parser.parse(home_page.content)
            rendered = renderer.render(parsed)

            menu = home_page.menu

            the_page = {
                'Title': home_page.title,
                'Content': rendered
            }
    else:
        template_path = "site/page.html"
        current_page = Page.query.filter_by(slug=slug).first()
        if current_page is None:
            abort(404)

        parsed = parser.parse(current_page.content)
        rendered = renderer.render(parsed)

        menu = current_page.menu

        the_page = {
            'Title': current_page.title,
            'Content': rendered
        }

    # Let's return the page and menu items
    return render_template(template_path, page=the_page, menu=menu,
                           settings=settings)


@pages_blueprint.app_errorhandler(404)
def page_not_found(e):
    # Get the menu we want to use for this page...
    # TODO: make this more dynamic... probably tie it to the page
    mainMenu = Menu.query.filter_by(name="Main").first()

    return render_template('404.html', menu=mainMenu), 404


@pages_blueprint.app_errorhandler(500)
def internal_server_error(e):
    # Get the menu we want to use for this page...
    # TODO: make this more dynamic... probably tie it to the page
    mainMenu = Menu.query.filter_by(name="Main").first()

    return render_template('500.html', menu=mainMenu), 500
