"""
Views for the file manager
"""

import os
import app
from datetime import datetime
from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask_login import login_required
from werkzeug.utils import secure_filename
from app import db
from app.models.File import File

# Init Blueprint
files_admin_blueprint = Blueprint('files_admin', __name__, template_folder='templates')

ALLOWED_EXTENSIONS = ["jpg", "png", "gif", "pdf", "doc" "docx", "xls", "xlsx"]


def allowed_file(filename):
    """Determine if the file is allowed"""
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@files_admin_blueprint.route('')
@login_required
def file_manager():
    """File Manager: Main Route"""
    return render_template('admin/file-manager/index.html',
                           js='file-manager/index')


@files_admin_blueprint.route('/upload', methods=['GET', 'POST'])
@login_required
def upload_file():
    """Upload a file"""
    if(request.method == 'POST'):
        uploaded_file = request.files['file']
        if(uploaded_file and allowed_file(uploaded_file.filename)):
            filename = secure_filename(uploaded_file.filename)
            uploaded_file.save(os.path.join(
                app.config['UPLOAD_DIR'], filename))

            file_record = File(name=filename,
                               path=app.config['UPLOAD_DIR'],
                               uploaded_date=datetime.utcnow())

            db.session.add(file_record)
            flash('"{0}" has been saved'.format(file_record.name))

            return redirect(url_for('.file_manager'))

    return render_template('admin/file-manager/upload.html',
                           js='file-manager/upload')
